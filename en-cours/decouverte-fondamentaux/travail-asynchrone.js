const boutonStop = document.querySelector('button#btnStop');


console.log("1. avant async");
// A faire quand on a un gros traitement, ou utiliser un WebWorker...
// Sinon on va bloquer le thread principal
setTimeout(function() {
    console.log('1. Je m\'éxécute en async', this);
}, 0);
console.log("1. après async");



console.log("2. avant async");
const handlerId = setInterval(function() {
    console.log('2. Je m\'éxécute en async', this);

    const unDiv = document.createElement('div');
    unDiv.style.border = '2px solid black';
    unDiv.style.width = '20px';
    unDiv.style.height = '20px';

    document.body.appendChild(unDiv);
}, 1000);
console.log("2. après async");


boutonStop.addEventListener('click', function() {
    clearInterval(handlerId);
});
