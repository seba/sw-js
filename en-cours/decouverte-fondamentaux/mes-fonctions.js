// "use strict";

/**
 * Un vaiseau qui vole
 */
function vaisseauTest() {
    console.log('je vole');

    return true;
}

vaisseauTest();

var vitesseDeBase = 15;

/**
 * Représente une class
 */
function Vaisseau(vitesseARecuperer = 20) {
    console.log('Appel du vaisseau');

    // Définie une variable globale
    this.vitesse = vitesseARecuperer;

    // attention Variable globale !
    vitesse = vitesseARecuperer + 10;

    // this est un pointeur sur le scope global
    console.log(this);
}

Vaisseau.prototype.voler = function() {
    console.log('ouiiinnnnn je vole !', this.vitesse);
}


console.log(this);
Vaisseau();

// on peut appeler la variable défini dans la fonction
console.log(vitesse)

// Attention le new crée un nouveau contexte this
const unVaisseau = new Vaisseau(20);

unVaisseau.volerSuperMegaVite = function() {
    console.log('Fzioooo je vole trop vite!');
}
unVaisseau.volerSuperMegaVite()

// debugger;

const unVaisseau2 = new Vaisseau(30);

// unVaisseau.volerSuperMegaVite() Ca plante



console.log('vaiseau 20 vitesse', unVaisseau.vitesse);
console.log('vaiseau 30 vitesse', unVaisseau2.vitesse);

console.log('vitesse', this.vitesse);
console.log('this vitesse', this.vitesse);
console.log('global vitesse', window.vitesse);
console.log('globalThis vitesse', globalThis.vitesse); // ECMA 11 pour compatibilité nav et node

const unTableauDeVaisseau = [];
unTableauDeVaisseau.push(unVaisseau);
unTableauDeVaisseau.push(unVaisseau2);
unTableauDeVaisseau.push(new Vaisseau(80));
unTableauDeVaisseau.push(new Vaisseau());

unTableauDeVaisseau.forEach( element => {
    element.voler();
});
