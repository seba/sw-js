class Droid extends Personnage {
    /**
     * Droid se déplaçant dans le jeu
     * @param {string} matricule
     */
    constructor(matricule="", pointsDeVie=0, pointsDAttaque=10) {
        super(matricule, pointsDeVie, pointsDAttaque);

        this.element.classList.add('droid');
    }
}
