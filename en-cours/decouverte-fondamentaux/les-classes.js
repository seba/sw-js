/**
 * Un vaisseau
 */
class Vaisseau {
    // Plus obligé, on l'a décalré dans le constructeur
    //longueur = 100;
    // ES11
    #ordinateurDeBord = {};

    //ES 6
    constructor(uneLongueur = 100) {
        this.longueur = uneLongueur;
    }

    // On peut avoir des fonctions privées
    // #voler() {
    voler() {
        console.log('Je vole', this);
    }

    // ES6
    /**
     * Getter
     */
    get ordinateurDeBord() {
        return this.#ordinateurDeBord;
    }

    /**
     * Setter
     */
    set ordinateurDeBord(value) {
        this.#ordinateurDeBord = value;
    }
}

const unVaisseau = new Vaisseau(120);

console.log(unVaisseau);
console.log(unVaisseau.longueur);

unVaisseau.voler();

// Plante
//console.log('ordi', unVaisseau.#ordinateurDeBord)
console.log('ordi', unVaisseau.ordinateurDeBord)
unVaisseau.ordinateurDeBord = {puissance: 120}



class xWing extends Vaisseau {
    constructor(puissanceDeFeu, laLongueur) {
        super(laLongueur);
    }

    voler() {
        super.voler(); // pas obligatoir
        console.log('Je vole (XWing)', this);
    }

    sePoser() {
        super.voler(); // parfaitement autorisé
    }
}


const unXWing = new xWing();

unXWing.voler();
console.log(unXWing.longueur);



unVaisseau.voler.call(unXWing);

console.log(Vaisseau.prototype);

// Aussi dispo en ES5
unVaisseau['voler']();

for (const key in unVaisseau) {
    console.log(key);
    console.log(unVaisseau[key]);

    // plante
    // key = "pouet";

    unVaisseau[key] = 8
}


// Ne pas faiore INTERDIT !!!
eval('unVaisseau.voler()');
//eval(prompt("hack"));
