// Singleton
const monWookie = {
    prenom: 'Chewie',
    armes: [
        {
            type: 'blaster',
            puissance: 10
        },
        {
            type: 'pistolet laser',
            puissance: 20
        }
    ],
    crier: function() {
        console.log('je crie', this);
    }
}

console.log(monWookie.prototype);
monWookie.crier();

function faireCrier(unPersonnage) {
    unPersonnage.crier();
}

function Chien() {
    let tatouage = '123XZ';

    // Closure : fonction à l'interieur d'une fonction
    // permet d'accéder à une variable "privée" comme tatouage
    this.crier = function () {
        console.log('wafff', this, tatouage);
    }

    this.getTatouage = function() {
        return tatouage;
    }
}

const monChien = new Chien();
monChien.crier();
console.log('Tataouage monchien', monChien.getTatouage())

faireCrier(monWookie);
faireCrier(monChien);
