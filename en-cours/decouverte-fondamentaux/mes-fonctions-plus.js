function Ewok(taille) {
    this.taille = taille;
}

function roulerEnBoule() {
    console.log("Rouler en boule", arguments[0]);
}

roulerEnBoule(15);
roulerEnBoule();


function crier(message) {
    console.log(this, message);
}

Ewok.prototype.crier = crier;

const monEwok = new Ewok(120);
const monEwokEnDouble = monEwok;
monEwok.crier();

// monEwok = new Ewok(120); impossible du au const
function Wookie() {
}

const unWookie = new Wookie();

// Wookie.prototype.crier = crier;
// unWookie.crier('roaar');

monEwok.crier.call(unWookie, 'Roaarr');

crier.call(monEwok, 'outini 2');
crier.call(unWookie, 'roarr 2');

let crierAvecContexte = crier.bind(unWookie, 'roarrr 3');
crierAvecContexte();

crierAvecContexte = crier.bind(monEwok, 'outini 3');
crierAvecContexte()
