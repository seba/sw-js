class Jedi extends Personnage {
    #pointsDeForce;
    constructor(prenom="", pointsDeForce=100, pointsDeVie=1000, pointsDAttaque=10) {
        super(prenom, pointsDeVie, pointsDAttaque);
        this.#pointsDeForce = pointsDeForce;

        this.element.classList.add('jedi');
    }
}
