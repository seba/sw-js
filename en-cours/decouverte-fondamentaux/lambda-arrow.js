function hurler(message) {
    console.log(message);
}

hurler('roaaar');
const loupGarou = new hurler("Aouuh");



const hurlerLambda1 = message => console.log(message);
hurlerLambda1('1. roaar');


// ca plante : const loupGarou1 = new hurlerLambda1('1. aououou')


const hurlerLambda2 = (message, nbTime) => console.log(message);
hurlerLambda2('2. roaar');

const sansParam = () => console.log('lalala');

const hurlerLambda3 = message => {
    console.log(message);
    console.log('une seconde ligne');
}
hurlerLambda3('3. roooar');



const loupGarouSingleton = {
    hurler: function() {
        console.log("ahouuou", this);
    },
    // on perd le contexte
    manger : () => {
        console.log('je  dévore tout !', this);
    }

}

loupGarouSingleton.hurler();
loupGarouSingleton.manger();


const lesLoups = [];
lesLoups.push({
    taille: 130,
    couleurPelage: 'gris'
});
lesLoups.push({
    taille: 120,
    couleurPelage: 'noir'
});
lesLoups.push({
    taille: 150,
    couleurPelage: 'gris'
});

const lesLoupsGris = lesLoups.filter(item => item.taille == '130');
const lesLoupsGris = lesLoups.filter(item => item.taille === '130'); // compare avec le type => vide


// regarder !! dans les notes

const monBtnStart = document.querySelector("btnStart");

// monBtnStart.addEventListener('click', function()  {
//     console.log('click', this);
// }


// la lambda récupère le contexte de l'appelant : ici
// let pouet = 2 ;
monBtnStart.addEventListener('click', () => {
    console.log('click', this);
    // Ca marche ça:
    // console.log(pouet);
}



