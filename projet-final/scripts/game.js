class Game {
    width = 640;
    height = 480;
    #divArea;
    troopers = [];
    droids = [];
    jedi;

    constructor() {
        this.area.style.width = `${this.width}px`;
        this.area.style.height = `${this.height}px`;
    }

    /**
     * Crée les personnages
     */
    init(nombreTroopers = 10, nombreDroides = 10) {

        this.troopers = []

        for( let i=0; i<nombreTroopers; i++)
        {
            const monTrooper = new Trooper('123456', 100);
            const x = Math.floor(Math.random() * Math.floor(this.width));
            const y = Math.floor(Math.random() * Math.floor(this.height));
            monTrooper.deplacer({x: x, y: y});
            this.troopers.push(monTrooper);
            this.area.appendChild(monTrooper.element);
        }

        this.unDroid = new Droid('45687', 150);
        for( let i=0; i<nombreDroides; i++)
        {
            const monDroid = new Droid('123456', 100);
            const x = Math.floor(Math.random() * Math.floor(this.width));
            const y = Math.floor(Math.random() * Math.floor(this.height));
            monDroid.deplacer({x: x, y: y});
            this.droids.push(monDroid);
            this.area.appendChild(monDroid.element);
        }
        const monJedi = new Jedi('Luke', 100, 1500);
        const x = Math.floor(Math.random() * Math.floor(this.width));
        const y = Math.floor(Math.random() * Math.floor(this.height));
        monJedi.deplacer({x: x, y: y});
        this.jedi = monJedi;
        this.area.appendChild(monJedi.element);
    }

    deplacerObjet(perso) {
        const x = Math.floor(Math.random() * Math.floor(this.width));
        const y = Math.floor(Math.random() * Math.floor(this.height));
        perso.deplacer({x: x, y: y});
    }

    start() {
        for(const perso of this.troopers.concat(this.droids)) {
            if(!perso.handler) {
                perso.handler = setInterval(this.deplacerObjet.bind(this, perso), 20);
            }
        }
    }

    stop() {
        for(const perso of this.troopers.concat(this.droids)) {
            if(perso.handler) {
                clearInterval(perso.handler);
                perso.handler = 0;
            }
        }

    }


    get area() {
        if(!this.#divArea)
        {
            this.#divArea = document.getElementById('game');
        }

        return this.#divArea;
    }
}
