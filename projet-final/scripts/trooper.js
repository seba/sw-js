class Trooper extends Personnage {
    constructor(matricule="", pointsDeVie=0, pointsDAttaque=10) {
        super(matricule, pointsDeVie, pointsDAttaque);

        this.element.classList.add('trooper');
    }
}
