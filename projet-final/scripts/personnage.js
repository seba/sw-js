/**
 * Classe générique de personnage du jeux
 */
class Personnage {
    #matricule;
    #position;
    #pointsDAttaque;
    #pointsDeVie;
    #element;
    handler;

    constructor(matricule, pointsDeVie=1000, pointsDAttaque=10) {
        this.matricule = matricule;
        this.#pointsDeVie = pointsDeVie; // TODO utiliser setter
        this.#pointsDAttaque = pointsDAttaque;

        this.#position = new Vecteur(0,0);

        let element = document.createElement('div');
        element.classList.add('personnage');
        this.#element = element;
    }

    get element() {
        return this.#element;
    }
    get matricule() {
        return this.#matricule;
    }

    set matricule(value) {
        if(!value) {
            throw new Error('le matricule doit avoir une valeur');
        }
        this.#matricule = value;
    }

    /**
     * @param vecteur {Vecteur} nouvelle position
     */
    deplacer(vecteur) {
        // console.log("Deplace", this, vecteur.x, vecteur.y);
        this.#position = vecteur;
        this.element.style.left = `${vecteur.x}px`;
        this.element.style.top = `${vecteur.y}px`;
    }

    attaquer(enemy) {
        console.log(this, "attaque", enemy);
        enemy.recoitAttaque(this.#pointsDAttaque);
    }

    recoitAttaque(pointsDomage) {
        if(!this.estEnVie){
            console.warn("impossible d'attaquer", this, "est mort");
            return false
        }

        this.#pointsDeVie -= pointsDomage;
        console.log(this, "points de vie restant", this.#pointsDeVie)

        if(! this.estEnVie) {
            console.log(this, "est mort");
        }
    }

    get estEnVie() {
        return this.#pointsDeVie > 0;
    }

}
