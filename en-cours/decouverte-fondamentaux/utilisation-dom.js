const unDiv = document.getElementById('container'); 

unDiv.innerText = "Ah que coucou";
unDiv.innerHTML = "<b>Ah que coucou<b>";

const titre = 'Mon super site des années 80';

unDiv.innerHTML = 
`
    <div>${titre}</div>
    <div>${titre}</div>
`;

console.log(unDiv)


const unNouveauDiv = document.createElement('div');
unNouveauDiv.className = 'carre debug';
unNouveauDiv.style.width = '20px';
unNouveauDiv.style.height = '30px';
unNouveauDiv.innerText = ':=)';

// unNouveauDiv.style.position = 'absolute';
unNouveauDiv.style.left = '100px'
unNouveauDiv.style.top = '10px';

unDiv.appendChild(unNouveauDiv);

const maFonction = function() {
    console.log('je suis cliqué');
}

const buttonStart = document.getElementById('btnStart');
buttonStart.addEventListener('click', maFonction);

// Le coupe click / mafonction est unique
buttonStart.addEventListener('click', maFonction);
buttonStart.addEventListener('click', function() {
    console.log('ouh yeah');
});

// plus de listener
// buttonStart.removeEventListener('click', maFonction);
